package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.items.ItemHandlerHelper;
import org.junit.Test;

import javax.swing.plaf.basic.BasicComboBoxUI;

public class ItemLoreHandlerTest {

    @Test
    public void SplitLore_WithOneLine_WorksFine() {
        String test = "§eLorem Ipsum is simply dummy text of the printing";
        NBTTagList tagList = ItemLoreHandler.splitLore(test, 60);
        System.out.println(tagList);

        String result = "[\"§eLorem Ipsum is simply dummy text of the printing\"]";
        assert tagList.toString().equals(result);
    }

    @Test
    public void SplitLore_WithThreeLines_WorksFine() {
        String test = "§eLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
        NBTTagList tagList = ItemLoreHandler.splitLore(test, 60);
        System.out.println(tagList);

        String result = "[\"§eLorem Ipsum is simply dummy text of the printing and typesetting\",\"§eindustry. Lorem Ipsum is simply dummy text of the printing\",\"§eand typesetting industry.\"]";
        assert tagList.toString().equals(result);
    }

    @Test
    public void SplitLore_WithThreeLinesAndTwoColors_WorksFine() {
        String test = "§eLorem Ipsum is simply dummy text of the printing and §3typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
        NBTTagList tagList = ItemLoreHandler.splitLore(test, 60);
        System.out.println(tagList);

        String result = "[\"§eLorem Ipsum is simply dummy text of the printing and §3typesetting\",\"§3industry. Lorem Ipsum is simply dummy text of the printing\",\"§3and typesetting industry.\"]";
        assert tagList.toString().equals(result);
    }

}