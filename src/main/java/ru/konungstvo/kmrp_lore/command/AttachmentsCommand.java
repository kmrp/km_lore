package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.AttachmentsHandler;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AttachmentsCommand extends CommandBase {
    @Override
    public String getName() {
        return "attachment";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //
        }
        String attachmentName;
        EntityPlayerMP player = getCommandSenderAsPlayer(sender);
        ItemStack attachmentPoint = ((EntityPlayerMP) sender).getHeldItemMainhand();
        AttachmentsHandler attachmentsHandler = new AttachmentsHandler(attachmentPoint);
        switch (args[0]) {
            // Аттачменты
            case "find":
                attachmentName = args[1];
                attachmentsHandler.getAttachableFromInventory(attachmentName, player);
                return;
            case "attach":
                attachmentName = args[1];
                attachmentsHandler.attach(attachmentPoint, attachmentName, player);
                return;
            case "detach":
                attachmentName = args[1];
                try {
                    attachmentsHandler.detach(attachmentPoint, attachmentName, player);
                } catch (LoreException e) {
                    e.printStackTrace();
                }
                return;
            case "setattachmentpoint":
                String attachmentPointString = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                NBTTagCompound attachmentPointNBT;
                try {
                    attachmentPointNBT = JsonToNBT.getTagFromJson(attachmentPointString);
                } catch (NBTException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                    return;
                }
                attachmentsHandler.setAttachmentPoint(attachmentPointNBT);
                return;
            case "setattachable":
                String attachableString = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                NBTTagCompound attachableNBT;
                try {
                    attachableNBT = JsonToNBT.getTagFromJson(attachableString);
                } catch (NBTException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                    return;
                }
                attachmentsHandler.setAttachable(attachableNBT);
                return;
            case "setattachments":
                String attachmentsString = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                NBTTagCompound attachmentsNBT;
                try {
                    attachmentsNBT = JsonToNBT.getTagFromJson(attachmentsString);
                } catch (NBTException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                    return;
                }
                attachmentsHandler.setAttachments(attachmentsNBT);
                return;

                // Патроны
            case "setprojectile":
                String projectileString = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                NBTTagCompound projectileNBT;
                try {
                    projectileNBT = JsonToNBT.getTagFromJson(projectileString);
                } catch (NBTException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                    return;
                }
                attachmentsHandler.setProjectile(projectileNBT);
                TextComponentString answer = new TextComponentString("Лор патронов записан.");
                answer.getStyle().setColor(TextFormatting.GRAY);
                sender.sendMessage(answer);
                return;
            case "loadtoclip":
                String projectileName = args[1];
                attachmentsHandler.loadProjectileToClip(projectileName, player);
                return;

                // Стрельба
            case "fire":
                String weaponTagName = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                attachmentsHandler.fireProjectile(weaponTagName);
                return;

                // Дебаг
            case "get":
                ArrayList<String> attachables = attachmentsHandler.getAttachablesListFromInventory(player);
                for (String attachable : attachables) {
                    TextComponentString show = new TextComponentString(attachable);
                    show.getStyle().setColor(TextFormatting.GRAY);
                    sender.sendMessage(show);
                }
                return;
        }
    }
}