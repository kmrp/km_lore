package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;

import java.util.Arrays;

public class WeaponTagsCommand extends CommandBase {
    public static final String NAME = "weapontag";

    @Override
    public String getName() { return NAME; }

    @Override
    public String getUsage(ICommandSender sender) { return null; }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //
        }
        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();

        TextComponentString result;

        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(item);

        switch (args[0]) {
            case "add":
                if (!(PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get()) || PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.CM.get()))) return;
                if (args.length < 3) {
                    // TODO
                }
                String weapontagString = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                NBTTagCompound weapontag;
                try {
                    weapontag = JsonToNBT.getTagFromJson(weapontagString);
                } catch (NBTException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                    return;
                }
                weaponTagsHandler.addWeaponTag(weapontag);

                result = new TextComponentString("Лор оружия записан.");

                //DEBUG
                //result.appendSibling(new TextComponentString("\n"+weapontagString));
                //result.appendSibling(new TextComponentString("\n"+weapontag.toString()));

                result.getStyle().setColor(TextFormatting.GRAY);
                sender.sendMessage(result);
                return;
            case "del":
                if (!(PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get()) || PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.CM.get()))) return;
                if (args.length < 2) {
                    // TODO
                }

                String weapon = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                try {
                    weaponTagsHandler.delWeaponTag(weapon);
                    result = new TextComponentString("Лор оружия удалён.");
                    result.getStyle().setColor(TextFormatting.GRAY);
                    sender.sendMessage(result);
                } catch (LoreException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                }
                return;
            case "show":
                if (!(PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get()) || PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.CM.get()))) return;
                if (item.getTagCompound() == null) {
                    result = new TextComponentString("TagCompound пуст");
                    sender.sendMessage(result);
                    return;
                }
                result = new TextComponentString(item.getTagCompound().toString());
                sender.sendMessage(result);
                return;
            case "change":
            case "default":
                weaponTagsHandler.setDefaultTagIfNone();
                weaponTagsHandler.changeDefaultWeaponToNext();
                result = new TextComponentString("Default weapon changed to " + weaponTagsHandler.getDefaultWeaponName());
                result.getStyle().setColor(TextFormatting.GRAY);
                sender.sendMessage(result);
                return;

        }
    }
}
