package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;

public class WeaponDurabilityCommand extends CommandBase {
    @Override
    public String getName() {
        return "durability";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get()) || PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.CM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //
        }
        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();

        WeaponDurabilityHandler weaponDurabilityHandler = new WeaponDurabilityHandler(item);
        String weapon;
        switch (args[0]) {
            case "takeaway":
                int loss;
                if (args.length < 2) {
                    loss = 1;
                } else {
                    loss = Integer.parseInt(args[1]);
                }
                weaponDurabilityHandler.takeAwayDurability(loss);
                return;
            case "refill":
                int gain;
                gain = Integer.parseInt(args[1]);
                weaponDurabilityHandler.refillDurability(gain);
                return;
            case "setmax":
                int max = Integer.parseInt(args[1]);
                weaponDurabilityHandler.setMaxDurability(max);
                return;
            case "set":
                int dur = Integer.parseInt(args[1]);
                weaponDurabilityHandler.setDurability(dur);
                return;
            case "show":
                TextComponentString show = new TextComponentString(weaponDurabilityHandler.getDurabilityDict().toString());
                show.getStyle().setColor(TextFormatting.GRAY);
                sender.sendMessage(show);
                return;

        }
    }

}