package ru.konungstvo.kmrp_lore.command;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.ClientProxy;
import ru.konungstvo.kmrp_lore.helpers.Permissions;

public class NRPCommand extends CommandBase {
    @Override
    public String getName() {
        return "nrp";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
            return true;
    }



    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        TextComponentString answer = new TextComponentString("");
        if (args.length == 0) {
            answer = new TextComponentString("Варианты: sign, tools, fire, bigsign");
            sender.sendMessage(answer);
            return;
        }
        switch (args[0]) {
            case "sign":
                ResourceLocation rl = new ResourceLocation("minecraft:sign");
                Item item = ForgeRegistries.ITEMS.getValue(rl);
                ItemStack itemStack = new ItemStack(item);
                itemStack.setCount(16);
                ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStack);

                ResourceLocation rl8 = new ResourceLocation("quark:glass_item_frame");
                item = ForgeRegistries.ITEMS.getValue(rl8);
                itemStack = new ItemStack(item);
                itemStack.setCount(64);
                ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStack);

                break;
            case "tools":
                ResourceLocation rl2 = new ResourceLocation("minecraft:diamond_pickaxe");
                Item item2 = ForgeRegistries.ITEMS.getValue(rl2);
                ItemStack itemStack2 = new ItemStack(item2);
                itemStack2.setCount(1);
                itemStack2.setStackDisplayName("§4НРП");
                itemStack2.addEnchantment(Enchantment.getEnchantmentByID(33), 1);
                ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStack2);

                rl2 = new ResourceLocation("minecraft:diamond_axe");
                item2 = ForgeRegistries.ITEMS.getValue(rl2);
                itemStack2 = new ItemStack(item2);
                itemStack2.setCount(1);
                itemStack2.setStackDisplayName("§4НРП");
                itemStack2.addEnchantment(Enchantment.getEnchantmentByID(33), 1);
                ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStack2);

                break;

            case "bigsign":
                ResourceLocation rl3 = new ResourceLocation("bibliocraft:fancysign");
                Item item3 = ForgeRegistries.ITEMS.getValue(rl3);
                ItemStack itemStack3 = new ItemStack(item3);
                ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStack3);
                break;
            case "fire":
                ResourceLocation rl4 = new ResourceLocation("minecraft:flint_and_steel");
                Item item4 = ForgeRegistries.ITEMS.getValue(rl4);
                ItemStack itemStack4 = new ItemStack(item4);
                itemStack4.setStackDisplayName("§4НРП");
                ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStack4);
                break;
            case "box":
                ResourceLocation rl5 = new ResourceLocation("minecraft:red_shulker_box");
                Item item5 = ForgeRegistries.ITEMS.getValue(rl5);
                ItemStack itemStack5 = new ItemStack(item5);
                itemStack5.setStackDisplayName("§4НРП");
                ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStack5);
                break;
        }


    }
}
