package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.ItemLoreHandler;
import ru.konungstvo.kmrp_lore.helpers.LoreChangeLogger;
import ru.konungstvo.kmrp_lore.helpers.Permissions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InloreCommand extends CommandBase {
    @Override
    public String getName() {
        return "inlore";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList();
        result.add("insertlore");
        return result;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length <= 1) {
            TextComponentString error = new TextComponentString("Слишком мало аргументов!");
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            return;
        }

        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
        ItemStack before = item.copy();
        ItemLoreHandler itemLoreHandler = new ItemLoreHandler(item);

        String lore = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        if (!lore.contains("&")) {
            lore = "&f" + lore;
        }

        int pos;
        try {
            pos = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            TextComponentString answer = new TextComponentString("После /inlore нужно указать номер строки числом.");
            answer.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(answer);
            return;
        }

        try {
            itemLoreHandler.insertLore(pos, lore);
        } catch (LoreException e) {
            TextComponentString error = new TextComponentString(e.toString());
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            return;
        }


        if (!PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get())) {
            String name = itemLoreHandler.getName();
            if (!name.contains("[?]"))
                name += " &7[?]";            itemLoreHandler.setName(name);
        }

        TextComponentString answer = new TextComponentString("Лор добавлен на строку " + pos + ".");
        answer.getStyle().

                setColor(TextFormatting.GRAY);
        sender.sendMessage(answer);
        LoreChangeLogger.logLoreChanges(sender.getName(), before, item);
    }
}
