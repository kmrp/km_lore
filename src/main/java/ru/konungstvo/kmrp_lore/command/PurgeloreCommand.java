package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.ItemLoreHandler;
import ru.konungstvo.kmrp_lore.helpers.LoreChangeLogger;
import ru.konungstvo.kmrp_lore.helpers.Permissions;

public class PurgeloreCommand extends CommandBase {
    @Override
    public String getName() {
        return "purgelore";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
        ItemStack before = item.copy();
        ItemLoreHandler itemLoreHandler = new ItemLoreHandler(item);

        TextComponentString answer;
        try {
            itemLoreHandler.purgeLore();
        } catch (LoreException e) {
            answer = new TextComponentString(e.toString());
            answer.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(answer);
            return;
        }


        if (!PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get())) {
            String name = itemLoreHandler.getName();
            if (!name.contains("[?]"))
                name += " &7[?]";            itemLoreHandler.setName(name);
        }
        answer = new TextComponentString("Лор удалён.");
        answer.getStyle().setColor(TextFormatting.GRAY);
        sender.sendMessage(answer);
        LoreChangeLogger.logLoreChanges(sender.getName(), before, item);
    }
}
