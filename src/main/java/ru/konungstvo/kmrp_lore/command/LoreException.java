package ru.konungstvo.kmrp_lore.command;

public class LoreException extends Exception {
    String message;

    public LoreException(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message;
    }
}
