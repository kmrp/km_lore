package ru.konungstvo.kmrp_lore.network;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class PacketMessage implements IMessage {
    // A default constructor is always required
    public PacketMessage(){}

    public String customPacket;
    public PacketMessage(String tag) {
        this.customPacket = tag;
    }

    @Override public void toBytes(ByteBuf buf) {
        // Writes the String into the buf
        ByteBufUtils.writeUTF8String(buf, customPacket);
    }

    @Override public void fromBytes(ByteBuf buf) {
        // Reads the String back from the buf. Note that if you have multiple values, you must read in the same order you wrote.
        customPacket = ByteBufUtils.readUTF8String(buf);
    }
}
