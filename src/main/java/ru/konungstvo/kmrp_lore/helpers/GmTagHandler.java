package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class GmTagHandler extends LoreHandler {
    private final static String COMPOUND_NAME = "gmtag";
    public static final String TAGS_PATH = "/srv/www/main_site/data/tagsDB";


    public GmTagHandler(ItemStack itemStack) {
        super(itemStack);
    }

    public String getGmTag() {
        if (tagCompound.hasKey(COMPOUND_NAME)) {
            return tagCompound.getCompoundTag(COMPOUND_NAME).getString("tag");
        }
        return "";
    }

    public boolean isHidden() {
        NBTTagCompound tagTags = getTagTagsOrCreate();
        return tagTags.getBoolean("hidden");

    }

    public NBTTagCompound getTagTagsOrCreate() {
        if (tagCompound.hasKey(COMPOUND_NAME)) {
            return tagCompound.getCompoundTag(COMPOUND_NAME);
        }
        NBTTagCompound tagTags = new NBTTagCompound();
        this.tagCompound.setTag(COMPOUND_NAME, tagTags);
        return tagTags;
    }

    public void setGmTag(NBTTagCompound tag) {
//        tagCompound.merge(tag);
        this.tagCompound.setTag(COMPOUND_NAME, tag);
        System.out.println("Tag set, result: " + this.tagCompound);
    }

    public static boolean hasGmTag(ItemStack itemStack) {
        return itemStack.hasTagCompound() && itemStack.getTagCompound().hasKey(COMPOUND_NAME);
    }


    public void removeTag() {
        tagCompound.removeTag(COMPOUND_NAME);
    }

    public static JSONObject readJson(String filename) throws IOException, ParseException {
        FileReader reader = null;
        try {
            reader = new FileReader(filename);
        } catch (FileNotFoundException e) {
            return new JSONObject();
        }
        JSONParser jsonParser = new JSONParser();
        return (JSONObject) jsonParser.parse(reader);
    }

    @SideOnly(Side.SERVER)
    public static String getStringFromTag(String name) throws IOException, ParseException {
        JSONObject obj = readJson(String.format("%s/%s.tag", TAGS_PATH, name)) ;
        String taglore = (String) obj.get("taglore");
        return taglore;
    }

}
