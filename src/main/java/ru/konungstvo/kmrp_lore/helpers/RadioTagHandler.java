package ru.konungstvo.kmrp_lore.helpers;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagString;
import ru.konungstvo.kmrp_lore.command.LoreException;

import java.util.ArrayList;
import java.util.List;

public class RadioTagHandler extends LoreHandler {

    public RadioTagHandler(ItemStack itemStack) { super(itemStack); }
    public static final String RADIO_COMPOUND = "radio";

    public NBTTagCompound getRadioTagOrCreate() {
        if (tagCompound.hasKey(RADIO_COMPOUND)) {
            return tagCompound.getCompoundTag(RADIO_COMPOUND);
        }
        NBTTagCompound radioTag = new NBTTagCompound();
        this.tagCompound.setTag(RADIO_COMPOUND, radioTag);
        return radioTag;
    }

    public static boolean hasRadioTag(ItemStack itemStack) {
        if (itemStack.getTagCompound() == null) {
            return false;
        }
        return itemStack.getTagCompound().hasKey(RADIO_COMPOUND);
    }

    public boolean hasRadioTag() {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        return this.itemStack.getTagCompound().hasKey(RADIO_COMPOUND);
    }

    public NBTTagCompound getRadioTag(ItemStack itemStack) {
        if (itemStack.getTagCompound() != null) {
            if (itemStack.getTagCompound().hasKey(RADIO_COMPOUND)) {
                return itemStack.getTagCompound().getCompoundTag(RADIO_COMPOUND);
            }
        }
        return null;
    }

    public NBTTagCompound getRadioTag() {
        if (tagCompound.hasKey(RADIO_COMPOUND)) {
            return tagCompound.getCompoundTag(RADIO_COMPOUND);
        }
        return null;
    }

    public void removeRadioTag() {
        if (tagCompound.hasKey(RADIO_COMPOUND)) {
            tagCompound.removeTag(RADIO_COMPOUND);
        }
    }

    public void setRadioChannel(String channel) {
        if (tagCompound.hasKey(RADIO_COMPOUND)) {
            tagCompound.getCompoundTag(RADIO_COMPOUND).setTag("channel", new NBTTagString(channel));
        }
    }

    public String getRadioChannel (ItemStack itemStack) {
        if (getRadioTag(itemStack) != null){
            NBTTagCompound radioTag = getRadioTag(itemStack);
            if (radioTag.hasKey("channel")) {
                return radioTag.getString("channel");
            }
        }
        return null;
    }

    public String getRadioChannel () {
        if (getRadioTag() != null) {
            if (getRadioTag().hasKey("channel")){
                return getRadioTag().getString("channel");
            }
        }
        return null;
    }
}
